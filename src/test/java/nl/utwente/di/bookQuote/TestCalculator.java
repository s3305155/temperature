package nl.utwente.di.bookQuote;
import java.util.Queue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
        /***Tests the Quoter*/
public class TestCalculator {
    @Test
    public void testTemp ( ) throws Exception {
        Calculator calculator = new Calculator();
        double price = calculator.getTemperature(0);
        Assertions.assertEquals(32.0, price);
    }
}
